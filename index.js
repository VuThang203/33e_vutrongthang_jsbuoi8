var arrNum=[];

function nhapVaoMang(){
    var number = document.getElementById("txt-so-nguyen").value*1;
    if(document.getElementById("txt-so-nguyen").value.trim() == ""){
        return;
    }
    arrNum.push(number);
    document.getElementById("txt-so-nguyen").value = "";
    document.getElementById("result").innerHTML= /*html*/`
    <p class="text-info">Array: ${arrNum}</p>
    `;
}
// Cau 1:
function tongCacSoDuong(){
    var result = 0;
    for(var index=0;index<arrNum.length;index++){
        var number = arrNum[index];
        if(number>0){
            result+= number;
        }
    }
    document.getElementById("result-1").innerHTML= `Tổng các số dương là: ${result}`;
}
// Cau 2:
function demCacSoDuong(){
    var dem = 0;
    for(var index=0;index<arrNum.length;index++){
        var number = arrNum[index];
        if(number>0){
            dem++;
        }
    }
    document.getElementById("result-2").innerHTML= `Số lượng số dương là: ${dem}`;
}
// Cau 3:
function timSoNhoNhat(){
    var min = arrNum[0];
    for(var index= 0;index<arrNum.length;index++){
        if(min >arrNum[index]){
            min = arrNum[index];
        }
    }
    document.getElementById("result-3").innerHTML= `Số nhỏ nhất là: ${min}`;

}
// Cau 4:

function timSoMin(arr){
    var min = arr[0];
    for(var i = 0; i<arr.length ;i++){
        if(arr[i]>0){
            min = arr[i];
            break;
        }else{
            min = `Không có số dương nào`;
        }
    }
    return min;
}

function soDuongNhoNhat(){
    for(var index= 0;index<arrNum.length;index++){
        var min = timSoMin(arrNum);
        if(min >arrNum[index] && arrNum[index]>0){
            min = arrNum[index];
            break;
        }
    }
    document.getElementById("result-4").innerHTML= `Số nguyên dương nhỏ nhất là: ${min}`;

}


// Cau 5:


function soChanCuoiCung(){
    var ketQua = -1;
    for(var index = 0; index<arrNum.length;index++){
        if(arrNum[index]%2==0){
            ketQua = arrNum[index];
        }
    }
    document.querySelector("#result-5").innerHTML= `Số chẵn cuối cùng là: ${ketQua}`;

}

// Cau 6:
function doiChoSo(){
    var number1 = document.querySelector("#vi-tri-mot").value*1;
    var number2 = document.querySelector("#vi-tri-hai").value*1;
    var change = 0;
        change = arrNum[number1];
        arrNum[number1] = arrNum[number2];
        arrNum[number2] = change;
    document.querySelector("#result-6").innerHTML = `Vị trí mới là ${arrNum}`;
}

// Cau 7:

function sapXepTangDan(){
    arrNum.sort(function(a, b){return a - b});
    document.querySelector("#result-7").innerHTML = `Giá trị tăng dần là: ${arrNum}`;
}

// Cau 8:

function kiemTraSoNguyenTo(number){
    var kiemTra = true;
    for(var i=2;i<=Math.sqrt(number);i++){
        if(number%i===0){
            kiemTra = false;
            break;
        }
    }
    return kiemTra;
}
function soNguyenDauTien(){
    var ketQua = "";
    for (var index =0;index<arrNum.length;index++){
        var kiemTra = kiemTraSoNguyenTo(arrNum[index]);
        if(kiemTra && arrNum[index]>1){
            ketQua = `Số nguyên tố đầu tiên là:` + arrNum[index];
            break;
        }else{
            ketQua = `Không có số nguyên tố`;
        }
    }
    document.getElementById("result-8").innerHTML = ketQua;

}

// Cau 9:

var arrNum1=[];
function themSoNguyen(){
    var number = document.querySelector("#txt-kiem-tra").value*1;
    if(document.querySelector("#txt-kiem-tra").value.trim() == ""){
        return;
    }
    arrNum1.push(number);
    document.querySelector("#txt-kiem-tra").value = "";
    document.querySelector("#result-9").innerHTML = /*html*/`
    <p class="text-info">Array: ${arrNum1}</p>
    `;
}

document.getElementById("demSoNguyen").onclick = function(){
    var dem = 0;
    for(var index = 0;index<arrNum1.length;index++){
        if(Number.isInteger(arrNum1[index])==true){
            dem ++;
        }
    }
    document.querySelector("#ketQua").innerHTML = `Số lượng số nguyên là: ${dem}`;
}

// Cau 10:
function soSanhAmDuong(){
    var kerQua = "";
    var soAm = 0;
    var soDuong = 0;
    for(var index = 0; index<arrNum.length;index++){
        if(arrNum[index]>0){
            soDuong++;
        }else{
            soAm++;
        }
    }
    if(soAm>soDuong){
        ketQua =`Số âm > Số dương`;
    }else if(soAm == soDuong){
        ketQua = `Số âm = Số dương`;
    }else{
        ketQua = `Số âm < Số dương`;
    }
    document.querySelector("#result-10").innerHTML = ketQua;
}